import java.util.Arrays;

/**
 * 
 * @author Joseph Odechie Bossman
 * 2nd December 2016
 * e-health Africa
 */
public class Main {

	public static void main(String[] args) throws Exception {
		
		System.out.println("**QUESTION 1**");
		/*
		 * 1. PERFECT NUMBERS
		 * 
		 * Given a number N return whether N is a perfect number or not. (A
		 * perfect number is a positive integer that is equal to the sum of its
		 * proper positive divisors excluding the number itself)
		 * if(isNumberPerfect(8128)) System.out.println("yes"); else
		 * System.out.println("no");
		 */
		 int inputValue = 6;
		 System.out.printf("is %s perfect : %s ",inputValue,isNumberPerfect(inputValue));
		 System.out.println(); //insert new line in between questions
		 
		 
		 
		 
		System.out.println("**QUESTION 2**");
		/*
		 * 2. ORDER OF STRING 1 Implement a function with signature
		 * find_chars(string1, string2) that takes two strings and returns a
		 * string that contains only the characters found in string1 and string2
		 * in the order that they are found in string1.
		 */
		String a = "ghanamustgo",b="ghanamustnotgo";
		char[] a1 = a.toCharArray(), b1 = b.toCharArray();
		char[] charArray = getCharInOrderOfA(a1, b1);
		printArray(a,charArray);
		System.out.println(); // insert new line in between questions
		
		
		
		
		

		System.out.println("**QUESTION 3**");
		/*
		 *Write a function that takes as input a sorted array and modifies the array to compact it, removing duplicates.
		 *Notes: The input array might be very large. 
		 */
		int[] arr = { 1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2, 3, 3,3,3,3,4,4,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,2000};
		printArray( removeDuplicates(arr));

	}

	/**
	 * checks whether input N is a perfect Number
	 * @param int N
	 * @return boolean
	 */
	private static boolean isNumberPerfect(int N) {
		// sum is initialized with 1 because 1 will always divide any numerator
		// consequently the startValue will have to be 2
		// N-1 because we need to exclude the number we are trying to determine
		// is perfect from the equation.
		// This will run in O(N) since it's linear
		int sum = 1, startValue = 2;
		boolean isPerfect = false;
		for (int i = startValue; i < N - 1; i++) {
			if (N % i == 0) {
				sum += i;
			}
		}
		return isPerfect = (sum == N) ? true : isPerfect;
	}

	/**
	 * Method to return a string
	 * @param string_A
	 * @param string_B
	 * @return char[]
	 */
	public static char[] getCharInOrderOfA(char[] string_A, char[] string_B) {
		String inOrderOf_A  = new String();

		for (int i = 0; i < string_A.length; i++)
			for (int j = 0; j < string_B.length; j++) {
				if (string_A[i] == string_B[j]) {
					if (inOrderOf_A.indexOf(string_A[i]) == -1)
						inOrderOf_A += string_A[i]; // <- characters are now stored in order of A
					break; // exit loop if order is done
				}
			}

		return inOrderOf_A.toCharArray();
	}
	
	
	/**
	 * method to remove duplicate elements in a sorted array
	 * @param int[] sortedArray
	 * @return int[]
	 */
	public static int[] removeDuplicates(int[] sortedArray) {
		int initIndex=0; // initial index
		sortedArray[initIndex]=sortedArray[0];
	    for(int i=0;i<sortedArray.length;i++)
	    {
	        if (sortedArray[initIndex]!=sortedArray[i])
	        {
	        	initIndex++; //counter keeps track of the number of unique items
	            sortedArray[initIndex]=sortedArray[i];
	        }
	    }
	    return Arrays.copyOf(sortedArray, initIndex+1); //index + 1 to consider the last element int the array.
	}
	
	/**
	 * method to print the elements in the array
	 * @param int[]
	 */
	public static void printArray(int[] input){
	    for( int i = 0; i < input.length; i++ ){
	        System.out.print(input[i] + " ");
	    }
	}
	
	
	/**
	 * method to print array elements after the char array has been returned
	 * @param String a
	 * @param char[] charArray
	 */
	public static void printArray(String a, char[] charArray){
		for (int i = 0; i < charArray.length; i++) {
			System.out.print(a.charAt(i));
		}
	}
}
