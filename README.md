# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repo is for posting answers to the code questions from ehealth Africa
* Version 1.1

### How do I get set up? ###
* platform independent
* No Configuration required
* Dependencies: Java runtime environment at least version 1.5
* No Database configuration required
* JUnit tests were not implemented. All tests are in the main class
* compile class using javac <name of file>, execute using java <class_name>
	

### Contribution guidelines ###

* Code review is welcome

### Who do I talk to? ###

*josephbossman@gmail.com
*